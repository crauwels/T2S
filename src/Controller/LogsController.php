<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class LogsController extends AppController
{	
	public function ranking($a){
	$this->loadModel('Members');
	
	$logs = $this -> Logs -> find("all", array("conditions"=>array("Logs.log_type"=>$a)));
	$member = $this -> Members -> find()->all()->toArray();


	$d = $this->Logs->find()->where(['log_type' => $a])->order(['log_value' => 'ASC']);
	$d = $d->select(['log_type','member_id', 's' => $d->func()->sum('log_value')])->group(['log_type','member_id'])->toArray();
	$premierID = "NULL";
	$premier = "NULL";
	$deuxiemeID = "NULL";
	$troisiemeID = "NULL";
	$deuxieme = "NULL";
	$troisieme = "NULL";
	if(sizeof($d)>0)
	{
		$premierID = $d[0]->member_id;
					foreach($member as $pm){
				if($pm->id==$premierID)
				{
					$emailCoupe = explode("@", $pm->email);
					$premier = $emailCoupe[0];
					
				}
			}
	}
	if(sizeof($d)>1)
	{
		$deuxiemeID = $d[1]->member_id;
					foreach($member as $dm){
				if($dm->id==$deuxiemeID)
				{
					$emailCoupe = explode("@", $dm->email);
					$deuxieme = $emailCoupe[0];
					
				}
			}
	}
	if(sizeof($d)>2)
	{
		$troisiemeID = $d[2]->member_id;
					foreach($member as $tm){
				if($tm->id==$troisiemeID)
				{
					$emailCoupe = explode("@", $tm->email);
					$troisieme = $emailCoupe[0];
					
				}
			}
	}
	
	$this -> set("logs", $d);
	$this -> set("premier", $premier);
	$this -> set("deuxieme", $deuxieme);
	$this -> set("troisieme", $troisieme);
	
	$this->set("member", $member);

	$this-> set('Rank', $a);
	}
	
	
	public function change(){
		
		switch($this->request->getData('Classement'))
    {
		case 0 : $this->redirect(['controller'=>'Logs','action' => 'ranking/Pompes']);
		break;
		
		case 1 : $this->redirect(['controller'=>'Logs','action' => 'ranking/Pas couru']);
		break;
		
		default : $this->redirect(['controller'=>'Logs','action' => 'ranking/Pompes']);
	}
	}
	
	public function addLog ($deviceID, $workoutID, $memberID, $logtype, $logvalue){
		
		$a = null;
		$this->loadModel('Devices');
		$this->loadModel('Workouts');
		$a = $this->Workouts->find("all", array("conditions"=>array("Workouts.id"=>$workoutID)))->toArray();
		$this->loadModel('Logs');
		
		$var = null;
		$var = $this->Devices->find("all", array("conditions"=>array("Devices.id"=>$deviceID, 'Devices.member_id'=> $this->Auth->User('id'), "Devices.trusted" => 1)));
		if(($var != null)&&($a != null)){
			$new = $this->Logs->newEntity();
			$new->member_id = $this->Auth->User('id');
			$new->workout_id = $workoutID;
			$new->device_id = $deviceID;
			$d = $this->Workouts->find()->where(['id'=>$workoutID]);
			$date = $d->select(['end_date']);	
			$new->date = $date;
			$new->log_type = $logtype;
			$new->log_value = $logvalue;
			$new->location_latitude = 10;
			$new -> location_logitude = 10;
			$this->Logs->save($new);
		}
		return $this->redirect('/Logs/ranking/Pompes');
	}
	
	public function releve($workoutID){
		$this-> set('workoutID',$workoutID);
	}
	
	public function addRelev($workoutID){
			$this->loadModel('Workouts');
			$logsTable = TableRegistry::get('Logs');
			$new = $logsTable->newEntity();

			$new->member_id = $this->Auth->User('id');
			$new->workout_id = $workoutID;
			$new->device_id = $this->request->getData('DeviceID');
			$d = $this->Workouts->find()->where(['id'=>$workoutID]);
			$date = $d->select(['end_date']);	
			$new->date = $date;			
			
			switch($this->request->getData('Log_Type')){
				
			case 0 : $new->log_type = 'Pompes';
					break;
			
			case 1 : $new->log_type = 'Pas couru';
					break;
					
			Default : return $this->redirect(['controller'=>'Seances','action' => 'classes']);
			
			}
			
			$new->log_value = $this->request->getData('Log_Value');
			$new->location_latitude = 10;
			$new -> location_logitude = 10;
			if($logsTable->save($new))
			{return $this->redirect(['controller'=>'Logs','action' => 'ranking/Pompes']);}
	}
}
