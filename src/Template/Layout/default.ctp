<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>

<head>
    <title>T2S</title>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<?= $this->Html->script('facebook.js'); ?>
	<?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('main.css') ?>
    <?= $this->Html->css('chat.css') ?>
    <?= $this->Html->css('seances.css') ?>
    <?= $this->Html->css('amis.css') ?>
    <?= $this->Html->css('compte.css') ?>
    <?= $this->Html->css('objets.css') ?>
    <?= $this->Html->css('classement.css') ?>
    <?= $this->fetch('css') ?>	
</head>

<body>

<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
    FB.init({
      appId            : '413170762465651',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v2.12',
	  oauth				: true
    });
  };

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=413170762465651&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


   jQuery(function($){
	   
	   $('.facebookConnect').click(function(){
		   
		   FB.login(function(response){
			var url = $(this).attr('href');
			//console.log(response);
			if(response.authResponse){
				window.location = url;
			}
		   }, {scope : 'email'});
		return false;
	   });
   });
</script>

	<div id="fb-root"></div>
    <nav class="navbar navbar-inverse" id="navigation">
	<div class="container-fluid">
    <div class="navbar-header">
	<div class=" navbar-brand">
	<?php
	$thumb_img = $this->Html->image('logoBarreNav (img+txt).png',array('alt'=>'', 'width' => '230'));

	echo $this->Html->link( $thumb_img, array('controller' => 'Accueil', 'action' => 'main'), array('escape'=>false));

	?>
	</div>
	</div>
	
    <ul class="nav navbar-nav">
	<?php if ($isConnected): ?>
      <li><?= $this->Html->link('Mon Compte', ['controller' => 'Members', 'action' => 'compte']);?></li>
      <li><?= $this->Html->link('Mes Objets Connectés', ['controller' => 'Devices', 'action' => 'connected']);?></li>      
      <li><?= $this->Html->link('Mes Séances', ['controller' => 'Seances', 'action' => 'classes']);?></li>

      <li><?= $this->Html->link('Amis', ['controller' => 'Bonds', 'action' => 'amis']);?></li>
	         <li><?= $this->Html->link('Messages', ['controller' => 'Messages', 'action' => 'chat']);?></li>

	<?php endif; ?> 
	  <li><?= $this->Html->link('Classement', ['controller' => 'Logs', 'action' => 'ranking/Pompes']);?></li>
    
	</ul>



    <ul class="nav navbar-nav navbar-right">
	<?php if (!$isConnected): ?>
      <li><?= $this->Html->link('Register', ['controller' => 'Members', 'action' => 'add'], array('class'=>'glyphicon glyphicon-user'));?></li>
      <li><?= $this->Html->link('Login', ['controller' => 'Members', 'action' => 'login'], array('class'=>'glyphicon glyphicon-log-in'));?></li>
    <?php else: ?>
		<li><?= $this->Html->link('Logout', ['controller' => 'Members', 'action' => 'logout'],array('class'=>'glyphicon glyphicon-log-out'));?></li>
	<?php endif; ?>
	</ul>

	</div>
    </nav>

	
    <?= $this->Flash->render() ?>
    <div >
        <?= $this->fetch('content') ?>
    </div>
	
	
    <footer>
    <div class="container" style="text-align: center;">
        <div class="media-container-row content mbr-white">
            <div class="col-12 col-md-2 mbr-fonts-style display-7">
                <p class="mbr-text">

                            <strong>L'équipe complète</strong>
                            <br>
                            <br>1 clic = 1 pompe
                            <br><a href="#"  class="mauve">Simon Auger</a>
                            <br><a href="#" class="mauve">Jérémy Bouhi</a>
                            <br><a href="#" class="mauve">Jef Crauwels</a>
                            <br><a href="#" class="mauve">Célestin Leroux</a>
                            <br>
							<br><strong>Options choisies</strong>
							<br><br>Réseau Social, Connexion Facebook et Bootstrap
                        </p>
                    </div>
                    <div class="col-12 col-md-2 mbr-fonts-style display-8">
                        <p class="mbr-text">
                            <strong>Liens utiles</strong>
                            <br>
							<br>
							<?= $this->Html->link('Utilisation du site', ['controller' => 'Accueil', 'action' => 'utilisation'],array('class'=>'mauve'));?>
							<br><?= $this->Html->link('Version.log', ['controller' => 'Accueil', 'action' => 'version'], array('class'=>'mauve'));?>
							<br><a href="http://rescord.fr/T2S" class="mauve">Site en Ligne</a>
                            <br>
                            <br><strong>Contact</strong>
                            <br>
                            <br>Envoie nous un <a href="#" class="mauve">email</a>
							<br>jef.crauwels@edu.ece.fr
							<br>jeremy.bouhi@edu.ece.fr
							<br>celestin.leroux@edu.ece.fr
							<br>simon.auger@edu.ece.fr
                            <br>


            </div>
            <div class="col-12 col-md-8">
                <div class="google-map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2625.3317651836587!2d2.2841719157075593!3d48.85188367928685!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e6701b4f58251b%3A0x167f5a60fb94aa76!2sECE+Paris!5e0!3m2!1sfr!2sfr!4v1517829977571" width="600" height="250" frameborder="0" style="border:0" allowfullscreen></iframe></div>
            </div>
        </div>
        <div class="footer-lower">
            <div class="media-container-row">
                <div class="col-sm-12">
                    <hr>
                </div>
            </div>
            <div class="media-container-row mbr-white">
                <div class="col-sm-12 copyright">
                    <p class="mbr-text mbr-fonts-style ">
                        © Copyright 2018 Travers2Sport - All Rights Reserved
                    </p>
                </div>

            </div>
        </div>
    </div>
	</footer>
</body>

</html>


<style>
.mauve
{
    text-decoration: none; 
    color: #8B008B; 
     
    }
.glyphicon 
    {
padding-top:15px; 
    }
</style>
