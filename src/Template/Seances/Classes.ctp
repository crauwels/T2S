<div class="row">
  <div id="enleverDerriereNav"></div>

  <div class="col-md-12" id="filSeance">
<div class = "nouvelleSeance">
    
    <h2 class="titre">Nouvelle Séance</h2>

    <?php 
    echo $this->Form->create('a', array('url'=>["controller"=>"Seances", "action"=>"ajout/"])); ?>
    
    <div class="ajoutSeance">

    <span>Type de sport</span>
    <?= $this->Form->select(
    'Sport',
    ['Tennis','Musculation', 'Course','Piscine','Football','Rugby','Basket'],['id'=>'Sport']);?>
      
      <?= $this->Form->input("Commentaire");?>
      <?= $this->Form->input("Localisation");?>
      <?= $this->Form->input("Date", ['type'=>'datetime-local']);?>
      
      <div id="boutonAj"><?= $this->Form->submit("Ajouter", ['class'=>'bouton']);?></div>
      <?= $this->Form->end();?>

    </div>
        <div class="scroll-down" > 
     <a class="btn js-scroll-trigger js-scrollTo" style="    font-size: 45px;
    line-height: 70px;
    width: 70px;
    height: 70px;
    padding: 0;
    letter-spacing: normal;
    color: #ffe4f4;
    border: 2px solid #ffe4f4;
    border-radius: 100% !important;
    -webkit-transition: all 0.5s;
    transition: all 0.5s;" href="#Messeances"> 
          <i class="fa fa-angle-down fa-fw" ></i>
      </a>
    </div>
</div>

</div>
</div>



<h2 class="titre" id="Messeances">Mes Séances</h2>
    <div id = "Messeance"><div class='row'>
    <?php foreach($seance as $seance)
      { 
      echo "
  <div class='col-md-3'>";
      echo "<div class='thumbnail'>";  
// echo $this->Html->image($seance->sport.'.jpg', ['alt' => '', 'class'=>'image']);
 echo  "<div class='caption'>";

      echo $this->Form->create('a', array('url'=>["controller"=>"Seances", "action"=>'modifier/'.$seance->id]));
       switch($seance->sport)
    {
        case "Tennis":
         echo "<h3 style='text-align: center;'><img src='https://png.icons8.com/ios/50/000000/tennis.png' style='width:1em;'>".$seance->sport."<img src='https://png.icons8.com/ios/50/000000/tennis.png' style='width:1em;'></h3>" ;
         break;

         case "Musculation":
         echo "<h3 style='text-align: center;'><img src='https://png.icons8.com/ios/50/000000/deadlift-filled.png' style='width:1em;'>".$seance->sport."<img src='https://png.icons8.com/ios/50/000000/deadlift-filled.png' style='width:1em;'></h3>" ;
         break;

         case "Course":
         echo "<h3 style='text-align: center;'><img src='https://png.icons8.com/ios/50/000000/running-filled.png' style='width:1em;'>".$seance->sport."<img src='https://png.icons8.com/ios/50/000000/running-filled.png' style='width:1em;'></h3>" ;
         break;

         case "Piscine":
         echo "<h3 style='text-align: center;'><img src='https://png.icons8.com/ios/50/000000/swimming.png' style='width:1em;'>".$seance->sport."<img src='https://png.icons8.com/ios/50/000000/swimming.png' style='width:1em;'></h3>" ;
         break;

         case "Football":
         echo "<h3 style='text-align: center;'><img src='https://png.icons8.com/ios/50/000000/football2-filled.png' style='width:1em;'>".$seance->sport."<img src='https://png.icons8.com/ios/50/000000/football2-filled.png' style='width:1em;'></h3>" ;
         break;
         case "Rugby":
         echo "<h3 style='text-align: center;'><img src='https://png.icons8.com/ios/50/000000/rugby-filled.png' style='width:1em;'>".$seance->sport."<img src='https://png.icons8.com/ios/50/000000/rugby-filled.png' style='width:1em;'></h3>" ;
         break;
         case "Basket":
         echo "<h3 style='text-align: center;'><img src='https://png.icons8.com/ios/50/000000/basketball-filled.png' style='width:1em;'>".$seance->sport."<img src='https://png.icons8.com/ios/50/000000/basketball-filled.png' style='width:1em;'></h3>" ;
         break;

         default: echo "<h3 style='text-align: center;'><img src='https://png.icons8.com/ios/50/000000/tennis.png' style='width:1em;'>".$seance->sport."<img src='https://png.icons8.com/ios/50/000000/tennis.png' style='width:1em;'></h3>" ; 
         
         
    }
       
echo "<ul>";  
      echo "<li><span class='glyphicon glyphicon-calendar'></span>".$seance->end_date."</li>" ;
	    echo "<li><span class='glyphicon glyphicon-globe'></span>".$seance->location_name."</li>" ;
      echo "<li>".$seance->description."</li>" ; 
echo "</ul>";
echo"<div style='text-align:center;'>";
      echo $this->Form->submit("Modifier",['class'=>'bouton']); 
      echo $this->Form->end();
	  echo $this->Form->create('a', array('url'=>["controller"=>"Logs", "action"=>"releve/".$seance->id]));
	  echo $this->Form->submit("Ajouter Relevé",['class'=>'bouton']); 
      echo $this->Form->end();
      
    echo "</div></div></div>
    </div>
  
";}?> 
  

</div>
<script>
  $(document).ready(function() {
    $('.js-scrollTo').on('click', function() { // Au clic sur un élément
      var page = $(this).attr('href'); // Page cible
      var speed = 750; // Durée de l'animation (en ms)
      $('html, body').animate( { scrollTop: $(page).offset().top }, speed ); // Go
      return false;
    });
  });
</script>
