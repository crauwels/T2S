<?php
namespace App\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\ORM\Table;
use Cake\ORM\Query;

class WorkoutsTable extends Table
{
    public function getLesDonneesDeLaSeance($WorkoutID, $select = array())
    {
    	$query = $this->find('all')
    		->select($select)
		    ->where(['id' => $WorkoutID])
		    ->first();


		return ($query->toArray());
    }

}
/*
    public function setLesDonneesDeLaSeance($workout)
	{

		$query = $workout->query();
		$query->update()
		    ->set(['published' => true])
		    ->where(['id' => $id])
		    ->execute();
	}
}*/
/* Utile pour la modification de la Table

$workout->date= $workout['date'];
		$workout->description= $workout['description'];
		$workout->lieu= $workout['location_name'];

		$this->save($workout);*/