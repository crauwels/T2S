<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\View\ViewBuilder;
use Cake\ORM\Query;
use Cake\I18n\Time;

class SeancesController extends AppController
{	
	public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    public function classes()
    {

    	$this -> loadModel("Workouts");
    	if (sizeof($this->Workouts->find()->all())>0)
      {
          $m = $this->Workouts->find()->where(['member_id'=> $this->Auth->User('id')])->all();
          $this->set("seance", $m->toArray()); 
      }
	}

  public function ajout($a)
  {
    $articlesTable = TableRegistry::get('Workouts');
    $article = $articlesTable->newEntity();

    $article->member_id=$this->Auth->User('id');
    $article->date = Time::now();
    $article->end_date = $this->request->getData('Date');
    $article->description = $this->request->getData('Commentaire');
    $article->location_name = $this->request->getData('Localisation');


    switch($this->request->getData('Sport'))
    {
        case 0:
         $article->sport = "Tennis";
         break;

         case 1:
         $article->sport = "Musculation";
         break;

         case 2:
         $article->sport = "Course";
         break;

         case 3:
         $article->sport = "Piscine";
         break;

         case 4:
         $article->sport = "Football";
         break;
         case 5:
         $article->sport = "Rugby";
         break;
         case 6:
         $article->sport = "Basket";
         break;

         default:
         $article->sport = "Tennis";
         
    }

    if ($articlesTable->save($article)) {

    }

    $this->redirect(['controller'=>'Seances','action' => 'classes']);

	}

	public function modifier($a)
	{
		$this -> loadModel('Workouts');

		$this-> set('WorkoutID',$a);

		$workout = $this->Workouts->getLesDonneesDeLaSeance($a);
		$this-> set('workout',$workout);  
	}

public function setLaSeance($workoutID)
{
			$workoutsTable = TableRegistry::get('Workouts');
			$new = $workoutsTable->newEntity();

			$new->member_id = $this->Auth->User('id');
			$new->id = $workoutID;
			$new->date = Time::now();
			$new->end_date = $this->request->getData('Date');
			$new-> location_name = $this->request->getData('Lieu');			
			$new-> description = $this->request->getData('Description');
			switch($this->request->getData('Sport'))
	{
        case 0:
         $new->sport = "Tennis";
         break;

         case 1:
         $new->sport = "Musculation";
         break;

         case 2:
         $new->sport = "Course";
         break;

         case 3:
         $new->sport = "Piscine";
         break;

         case 4:
         $new->sport = "Football";
         break;

         default:
         $new->sport = "Tennis";
         
    }

			if($workoutsTable->save($new))
			{return $this->redirect(['controller'=>'Seances','action' => 'classes']);}
}

	public function workoutParam($deviceID, $workoutID){

		$this->loadModel("Logs");
		$this->viewBuilder()->className('Json');
		
		if($this -> checkDevice($deviceID))
		{
			$query = $this->Logs->find("all", array("conditions"=>array("Logs.device_id"=> $deviceID, "Logs.workout_id" => $workoutID)));
			$workparam = $query -> select(["log_type","log_value"]);
			$this->set(array('WorkoutParameters' => $workparam, '_serialize' => array('WorkoutParameters')));
		}
		else{
		return $this->redirect(['controller' => "Accueil", "action" => "main"]);
		}
	}
	
	public function summary($deviceID){
	$this->loadModel("Workouts");
	$this->loadModel("Logs");

	if($this -> checkDevice($deviceID))
		{		
			$time = Time::Now();
			$query = 
			$this->Workouts->find('all', array(
				'conditions' => array(
				"Workouts.member_id" => $this->Auth->User('id'), "Workouts.end_date <" => $time), 'order' => 'Workouts.end_date DESC', 'limit' => 3));
			$summ = $query -> select(["end_date","description"]);
			$this->set(array('WorkoutSummaryPast' => $summ, '_serialize' => array('WorkoutSummaryPast')));
			
			/*$query2 = 
			$this->Workouts->find('all', array(
				'conditions' => array(
				"Workouts.member_id"=>$this->Auth->User('id'), "Workouts.end_date >" => $time), 'order' => 'Workouts.end_date DESC', 'limit' => 1));
			$summ = $query2 -> select(["end_date","description"]);
			$this->set(array('WorkoutSummaryFuture' => $summ, '_serialize' => array('WorkoutSummaryFuture')));*/
		} 
		else{
		return $this->redirect(['controller' => "Accueil", "action" => "main"]);
		}
	}
		
	public function checkDevice($deviceID){
	$this->loadModel("Devices");
	$check = $this -> Devices -> find("all", array( 'conditions' => array("Devices.trusted"=>1, "id" => $deviceID)));
	if(sizeof($check)>0){
		return true;
	}
	else{
		return false;
	}
	}
	
	public function classesamis($bondid){
		
		 $this -> loadModel("Workouts");
		 $this -> loadModel("Members");
    	if (sizeof($this->Workouts->find()->all())>0)
      {
          $m = $this->Workouts->find()->where(['member_id'=> $bondid])->all();
          $this->set("seance", $m->toArray()); 
          $ami = $this->Members->find()->where(['id'=> $bondid])->first();
          
          $emailCoupe = explode("@", $ami->email); 
          $this->set("ami", $emailCoupe[0]); 
      }
	}
}