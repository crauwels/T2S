<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

class MembersController extends AppController
{
        public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

         $this->Auth->config('authenticate', [
    'Basic' => ['userModel' => 'members'],
    'Form' => ['userModel' => 'members']
]);       
    }

     public function index()
     {
        $this->set('members', $this->Members->find('all'));
    }

    public function view($id)
    {
        $member = $this->Members->get($id);
        $this->set(compact('member'));
    }

    public function add()
    {        $member = $this->Members->newEntity();
        if ($this->request->is('post')) {
            $member = $this->Members->patchEntity($member, $this->request->getData());
            if ($this->Members->save($member)) {
            $member = $this->Auth->identify();
            if ($member) {
                $this->Auth->setUser($member);

                 return $this->redirect($this->Auth->redirectUrl());
            }
            }
        }
        $this->set('member', $member);
    }

    public function login()
    {
        if ($this->request->is('post')) {
            $member = $this->Auth->identify();
            if ($member) {
                $this->Auth->setUser($member);

                 return $this->redirect($this->Auth->redirectUrl());
            }

        }
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
	
	  public function compte()
    {
		$this -> loadModel("Members");
          $m = $this->Members->find("all", array("conditions"=>array("id"=>$this->Auth->User('id'))));
          $this->set("member", $m->toArray()); 
	  
        if ($this->request->is('post'))
        {
          $data=$this->request->getData();
          if ($data['email']!='')
          {
            if($this->Members->editEmail($data['email'], $this->Auth->user('id')))
            {
            }
            else {
            }
          }
          if ($data['password']!='')
          {
            if ($this->Members->editPassword($data['password'],  null, $this->Auth->user('id')))
            {
            }
            else {
            }
          }
			if ($data['submittedfile']!='')
			{
			move_uploaded_file($this->request->data['submittedfile']['tmp_name'], WWW_ROOT.'/img/'.$this->Auth->user('id').'.jpg');
			return $this->redirect(['controller' => 'Members', 'action' => 'compte']);
			}
			else{
			return $this->redirect(['controller' => 'Members', 'action' => 'compte']);
			}			
			
		return $this->redirect(['controller' => 'Members', 'action' => 'compte']);
		}
	}
	
	public function facebook(){
		  require('../../Facebook/Facebook.php');
			$facebook = new Facebook(array(
				'appID' => '413170762465651',
				'secret' => '9d7f01eb51d375cf51815b4db1e0de7d'
				));
				$member = $facebook->getUser();
				
				if($member){
					
						$infos = $facebook->api('/me');
						$d = array(
						'email' => $infos['email'],
						'password' => '');
						
						if($this -> Members -> save($d)){
							$this->redirect(['controller' => 'Members', 'action' => 'compte']);
						}
				}
		}
}