<section class="presentation">
      <div class="container">
          <h1 class="titre text-uppercase">Les squats c'est bon pour tes jambons!</h1>
          <?= $this->Html->image ('superpigBW.png', ['alt' => '', 'id'=>'superpig']);?>
          <h3>Déjà <?= $member?> personnes nous font confiance!</h3>
          
      </div>

</section>
	
	<section id="fonctionnement">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="titre text-uppercase">Services</h2>
          </div>
        </div>
        <div class="row text-center">
          <div class="col-md-4">
            <div class="rondImg"> 
  			<?= $this->Html->image('ioot.png', ['alt' => '', 'class'=>'TroisImg']);?>
           </div>
            <h4 class="service-heading">Mes Objets Connectés</h4>
            <p>Connectez vos accessoires avec notre salle pour pouvoir suivre vos résultats lors de vos seances</p>
          </div>
          <div class="col-md-4">
            <div class="rondImg">
			<?= $this->Html->image('trophy.png', ['alt' => '', 'class'=>'TroisImg']);?>
    </div>
            <h4 class="service-heading">Classement</h4>
            <p>Consultez le classement de tous les inscrits. Allez-vous être le premier?</p>
          </div>
          <div class="col-md-4">
            <div class="rondImg">
			<?= $this->Html->image ('workout.png', ['alt' => '', 'class'=>'TroisImg']);?>
    </div>
            <h4 class="service-heading">Mes séances</h4>
            <p>Nous vous proposons une grande diversité de séances. Choisissez celles qui vous interessent et inscrivez vous!</p>
          </div>
        </div>
      </div>  
    </section>
	
	  <section class="bg-light" id="team">
      <div class="container">
        


        <div class="row" >
                        <div class="heading-title text-center">
                           <h2 class="ext-uppercase">Notre super Team </h2>
                            <h3 class="p-top-30 half-txt">Dîtes adieu à la couenne grâce aux conseils des meilleurs coachs. </h3>
                        </div>

                        <div class="col-md-3 col-sm-3">
                            <div onmouseover="simonstock()" class="team-member">
                                <div class="team-img">
                                
                                    <?= $this->Html->image('simon1.png', ['alt' => '', 'class'=>'img-responsive', 'id'=>'simon']);?>
                                </div>
                                <div class="team-hover">
                                    <div class="desk">
                                        <h4>Whey Morray !</h4>
                                        <p>Ca te dis un petit verre, je suis célib'haltère ?</p>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="team-title">
                                <h5>Simon Auger</h5>
                                <span>Développeur du génie</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3"  >
                            <div  onmouseover="jefstock()" class="team-member" >
                                <div  class="team-img" >
                                    <?= $this->Html->image('jef1.png', ['alt' => '',  'id'=>'jef', 'class'=>'img-responsive']);?>
                                </div>
                                <div class="team-hover" >
                                    <div class="desk">  
                                        <h4>Salut!</h4>
                                        <p>Jeffrey remets nous du cochon!</p>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="team-title">
                                <h5 >Jef Crauwels</h5>
                                <span>Body Compiler</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div onmouseover="jeremstock()" class="team-member">
                                <div class="team-img">
                                    <?= $this->Html->image('jerem1.png', ['alt' => '', 'class'=>'img-responsive', 'id'=>'jeremy']);?>
 
                                </div>

                                <div class="team-hover">
                                    <div class="desk">
                                        <h4>Bang Bang</h4>
                                        <p>On va t'apprendre ce que c'est la motivation à travers le porc</p>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="team-title">
                                <h5>Jérémy Bouhi</h5>
                                <span>Codeur et Clubber</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div onmouseover="celestock()" class="team-member">
                                <div class="team-img">
                                    <?= $this->Html->image('celest1.png', ['alt' => '', 'class'=>'img-responsive', 'id'=>'celestin']);?>
                                </div>
                                <div class="team-hover">
                                    <div class="desk">
                                        <h4>Hey toi</h4>
                                        <p>Je suis ton coach pas ton pote ! On a pas élevé les cochons ensemble mon gars! </p>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="team-title">
                                <h5>Célestin Leroux</h5>
                                <span>Saucisson evangéliste</span>
                            </div>
                        </div>

                    </div>

                </div>
                <script type="text/javascript">
      

function jefstock()  {
    
   var jef = document.getElementById('jef');
   jef.src="/T2S/img/jef2.png";  

}
function celestock()  {
    
   var celestin = document.getElementById('celestin');
   celestin.src="/T2S/img/celest2.png";  
}
function simonstock()  {
    
   var simon = document.getElementById('simon');
   simon.src="/T2S/img/simon2.png";  
}
function jeremstock()  {
    
   var jerem = document.getElementById('jeremy');
   jerem.src="/T2S/img/jerem2.png";  
}


  

</script>
            

    </section>
