<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class DevicesController extends AppController
{
	
	public function index()
    {

    }
	
	public function connected(){
		$this -> loadModel("Devices");
		$d = $this -> Devices -> find("all", array("conditions"=>array("Devices.trusted"=>1, 'member_id'=>$this->Auth->User('id'))));
		$u = $this -> Devices -> find("all", array("conditions"=>array("Devices.trusted"=>0, 'member_id'=>$this->Auth->User('id'))));
		$this -> set("d", $d->toArray());
		$this -> set("u", $u->toArray());
	}
	
	public function valider($a)
{	
		
	$articlesTable = TableRegistry::get('Devices');
	$d = $articlesTable -> newEntity();
	$d->id = $a;
	$d->description = 'Objet Validé';
	$d->trusted = 1;
	
	if ($articlesTable->save($d)) {
		return $this->redirect(['controller'=>'Devices','action' => 'connected']);
    }
}

	public function supprimer($b){
		
	$this->request->allowMethod(['post', 'delete']);
	$id = $b;
    $devices = $this->Devices->get($id);
	if ($this->Devices->delete($devices)) {
		return $this->redirect(['controller'=>'Devices','action' => 'connected']);
	}
	}


}