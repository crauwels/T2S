<div class="row">

    <div class="col-md-12" >

        <div class="objetscov">

            <div class="ajoutSeance">
                <h2 class="titre">Objets connectés validés</h2>
            </div>

            <table>
                <?php foreach($d as $dev){
					echo $this->Form->create("b", array('url'=>["controller"=>"Devices", "action"=>"supprimer/".$dev->id]));
					echo"<tr><td>".$dev->serial."</td></tr>
					<tr><td><small>".$dev->description."</small></td>
					<td>".$this->Form->submit("Supprimer Objet")."</td></tr>";
					echo $this->Form->end();}?>
            </table>
        </div>

        <div class="objetsconv">

            <div class="ajoutSeance">
                <h2 class="titre">Objets connectés non validés</h2>
            </div>
                <table>
                    <?php 	
					foreach($u as $dev){
						echo $this->Form->create("a", array('url'=>["controller"=>"Devices", "action"=>'valider/'.$dev->id]));
						echo "<tr><td>".$dev->serial."</td></tr>
						<tr><td><small>".$dev->description."</small></td>	
						<td>".$this->Form->submit("Ajouter Objet")."</td></tr>";
						echo $this->Form->end();}?>   
                </table>
            </div>

            <audio controls autoplay class='validation'>
                <source src="../img/validation.mp3" type="audio/mpeg">
                Your browser does not support the audio element.
            </audio>
