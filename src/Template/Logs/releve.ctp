<section id="releve">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
             
     		<h2 class='intro-text'>Ajoute un releve à la seance numéro <?= $workoutID ?> </h2>
     	
          </div>
<div class="ajoutSeance" style="padding-bottom: 40px;">
	<?php
		echo $this->Form->create('a', array('url'=>["controller"=>"Logs", "action"=>"addRelev/".$workoutID]));		
		echo $this->Form->input('DeviceID');
		echo $this->Form->select('Log_Type', ['Pompes', 'Pas couru']);
		echo $this->Form->input('Log_Value');
		echo $this->Form->submit("Ajouter", ['class'=>'bouton']); 
		echo $this->Form->end();
	?>
</div>
        </div>
    
      </div>
</section>
