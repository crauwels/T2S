<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class MessagesController extends AppController
{
	public function chat(){
	$this->loadModel('Messages');
	$this->loadModel('Members');
	
	$message = $this ->Messages->find()->where(['member_id' => $this->Auth->User('id')])->
	orWhere(['member2_id'=>$this->Auth->User('id')]);
	$message = $message->order(['name','date' => 'DESC'])->all()->toArray();
	$this->set('message', $message);
	$this -> set("moi", $this->Auth->User('id'));
	$membre = $this->Members->find()->all()->toArray();
	$this->set("membre", $membre);
	
}

	public function envoyer(){
	
		$this->loadModel('Members');
	if (sizeof($this->Members->find()->where(['email' =>$this->request->getData('Destinataire')])->all())>0){
	$tinyint = 1;
	$articlesTable = TableRegistry::get('Messages');
    $article = $articlesTable->newEntity();

    $article->member_id = $this->Auth->User('id');
    $destinataire = $this->Members->find()->where(['email' =>$this->request->getData('Destinataire')]);
    $dest = $destinataire->select(['id']);
					
    $article->member2_id= $dest;
    $article->date = Time::Now();
    $article->name = $this->request->getData('Sujet');
    $article->description = $this->request->getData('Message');
	$article->lu = 0;

    $this->Messages->save($article);
}
    $this->redirect(['controller'=>'Messages','action' => 'chat']);

	}
		public function ajouter($sujet){
	//$this->loadModel("Messages");
	
	$articlesTable = TableRegistry::get('Messages');
    $article = $articlesTable->newEntity();
     $destinataire = $this->Messages->find();
    $dest = $destinataire->select(['member_id'])->where(['name' =>$sujet])->andWhere(['member_id !=' => $article->member_id = $this->Auth->User('id')])->first();
        if($dest!=null)
    {
    	$dest = $dest->member_id;
    }
    
    if($dest==null)
    {
    $destinataire = $this->Messages->find();
    $dest = $destinataire->select(['member2_id'])->where(['name' =>$sujet])->andWhere(['member2_id !=' => $article->member_id = $this->Auth->User('id')])->first(); 
    $dest=$dest->member2_id;

    }
    if($dest==null)
    {
    $dest="azerty@ece.fr";
    }
    //debug($dest);

    $article->member_id = $this->Auth->User('id');;
    $article->member2_id= $dest;
    
    $article->date = Time::Now();
    $article->name = $sujet;
    $article->description = $this->request->getData('Msg');
	$article->lu = 0;

    $this->Messages->save($article);

    $this->redirect(['controller'=>'Messages','action' => 'chat']);

	}
}
