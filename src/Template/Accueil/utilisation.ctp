<br><br><br><br><br>
<div  class="text-center">
<h1> Utilisation du site T2S </h1>

<pre>
#Quelques indications sur le fonctionnement de notre site

1. Messages
	
	Dans la table message il y a un attribut "read"
Le mot "read" étant réservé dans le SQL de CakePhp, nous ne pouvions pas mettre à jour la base de donnée.
	Solution => Nous avons changé localement le nom de l'attribu à "lu"
Merci de prendre en considération ce changement lors de votre correction.

Pour envoyer un message à un nouveau destinataire il faut remplir les inputs en haut de la page.
Il faut renseigner l'adresse mail du destinaire, indiquer un sujet et écrire un message.
Les messages sont classés par sujet.
Une fois le premier message envoyé, pour parler dans la discussion il faut aller dans la fenêtre de discussion et utiliser le bouton envoyer de celle-ci pour communiquer avec le destinataire.

2. API

	Il faut aller chercher les ID dans la base de donnée pour éxécuter les API

	API workoutparameters & getsummary => il faut mettre .json à la fin de l'url pour que les requêtes s'affichent comme il faut.
	
	Il est important de mettre les majuscules dans les noms des API lorsque l'on rentre l'URL.
	
	Pour les API "getsummary" et "workoutparameters" on ne peut récupérer que les informations de la personne actuellement connectée.
	Nous avons pris cette décision pour des raisons de sécurité. Nous imaginons que l'utilisateur doit d'abord se connecter à son compte pour pouvoir récupérer ses informations.

3. Connexion Facebook
	
	La connexion Facebook étant bloquée en local nous avons perdu énormément de temps sur ce point.
	Elle s'affiche donc en local mais ne fonctionne pas. 
	Merci de la tester en ligne.
	
4. Objets Connectés

	On ne peut ajouter un objet connecté qu'avec l'API "registerdevice"
</pre>
</div>