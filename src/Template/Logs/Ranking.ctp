<div class="ment">

<h2 class="titreRanking">Classement <?=$Rank?></h2>	


<div class="row">
	 <div class="col-md-4">  </div>
 <div class="col-md-4">  
<div class="competition-podium well">
  
  <div class="podium-block bronze">	
	<div class="name"><?=  $troisieme;?></div>
      <div class="podium"><span>3<sup>ème</sup></span></div>
    </div>
    <div class="podium-block gold">	
	<div class="name"><?=  $premier;?></div>
      <div class="podium"><span>1<sup>er</sup></span></div>
    </div>
    <div class="podium-block silver">	
	<div class="name"><?= $deuxieme;?></div>
      <div class="podium"><span>2<sup>ème</sup></span></div>
    </div>

</div>
</div>
</div>
 

<div class="row">

	<div class="col-md-4">  </div>
	 <div class="col-md-4"> 
	<div class="ajoutSeance">

		<?php 
		echo $this->Form->create('a', array('url'=>["controller"=>"Logs", "action"=>"change"])); ?>

		<span>Type de classement </span>
		<?= $this->Form->select('Classement',['Pompes', 'Pas couru']);?>     
		<?= $this->Form->submit("Afficher",['class'=>'bouton']);?>
		<?= $this->Form->end();?>
	</div>

	


</div>
</div>

<div class="row">
 <div class="col-md-3">  </div>
 <div class="col-md-6">  
	<table id="listeClassement" class="table">
		
	<thead>
    <tr>
      <th scope="col">Place</th>
      <th scope="col">Pseudo</th>
      <th scope="col">Score</th>
      
    </tr>
  </thead>
  <tbody>
  <?php $place=0;
		foreach($logs as $logs){$place++;
			echo"<tr>
      <th scope='row'>".$place."</th>
      <td>";
			foreach($member as $m){
				if($m->id==$logs->member_id)
				{
					$emailCoupe = explode("@", $m->email);
					echo $emailCoupe[0];
					if ($place==1) echo"<i class='fa fa-trophy' style='color:gold'></i>";
						if ($place==2) echo"<i class='fa fa-trophy' style='color:silver'></i>";
							if ($place==3) echo"<i class='fa fa-trophy' style='color:tan'></i>";
				}
			} echo "</td>
			
			<td>".$logs->s."</td>
			</tr>";
		}

		?>
</tbody>
	</table>
</div>
<div class="col-md-3"></div>
</div>


<script>
	$(document).ready(function () {
    function podiumAnimate() {
        $('.bronze .podium').animate({
            "height": "62px"
        }, 1500);
        $('.gold .podium').animate({
            "height": "165px"
        }, 1500);
        $('.silver .podium').animate({
            "height": "106px"
        }, 1500);
        $('.competition-container .name').delay(1000).animate({
            "opacity": "1"
        }, 500);
    }
    podiumAnimate();
});
</script>