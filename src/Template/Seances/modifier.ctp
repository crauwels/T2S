<section id="modifier">
  <div id="enleverDerriereNav"></div>

  <div class = "modifSeance">
      <div class="container">
        <div class="row">
          
          <div class="col-lg-12 text-center">
             
     		<h2 class='titre'>Séance numéro <?= $WorkoutID ?> </h2>
     	
          </div>

          <div class="ajoutSeance">

<?php
      echo $this->Form->create('j', array('url'=>["controller"=>"Seances", "action"=>"setLaSeance/".$WorkoutID]));?>

	  <p>Date actuelle : <?= $workout['end_date']?></p>
      <?php 
	  echo $this->Form->input('Date',
      		array(
        'type'=>'datetime-local'
    )) ;
	
	echo $this->Form->select('Sport',
    ['Tennis','Musculation', 'Course','Piscine','Football','Rugby','Basket'],['id'=>'Sport']);

    echo $this->Form->input('Lieu',
      		array(        
        'value' => $workout['location_name']
    )) ;

    echo $this->Form->input('Description',
      		array(        
        'value' => $workout['description'],       
    )) ;
      
      echo "<div id='boutonAj'>";
      echo $this->Form->submit("Modifier",['class'=>'bouton']); 
      echo "</div>";

      echo $this->Form->end();
      ?>
</div>
    </div>
        </div>
    
      </div>
</section>
