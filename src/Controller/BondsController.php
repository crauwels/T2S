<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class BondsController extends AppController
{
	    public function amis()
	    {
	    	//debug($this->Auth->User('id'));
	    	$this -> loadModel("Bonds");
	    	$this -> loadModel("Members");
	    	$a = $this->Bonds->find()->where(['member2_id'=>$this->Auth->User('id'),'trusted'=>0])->all()->toArray();
	    	$this -> set("a", $a);
	    	$membre = $this->Members->find()->all()->toArray();
	    	$this->set("membre",$membre);
	    	$b = $this->Bonds->find()->where(['member2_id'=>$this->Auth->User('id'),'trusted'=>1])->	orWhere(['member_id'=>$this->Auth->User('id'),'trusted'=>1])->all()->toArray();
	    	$this -> set("b", $b);
	    	//$c = $this ->Bonds->find()->select(['id'])->order(['id'=>'DESC']);
	    	//foreach($c as $c){debug($c->id);}
	    	$this -> set("moi", $this->Auth->User('id'));
	 		}
	    
	      public function ajout()
	      {
	      	$bond = TableRegistry::get('Bonds');
	    	$this -> loadModel("Members");
	    	$a = $this->Members->find()->where(['email'=>$this->request->getData('Email')])->all()->toArray();
	    	if (sizeof($a)>0 && ($this->request->getData('Email')!=$this->Auth->User('email')))
	    	{$b = 
		
	    	
	    	
    		$article = $bond->newEntity();
    		$article->member_id=$this->Auth->User('id');
    		$article->member2_id = $a[0]->id;
    		$article->trusted = 0; 
    		
    		if ($bond->save($article)) {
    			

    }
	      }

	      $this->redirect(['controller'=>'Bonds','action' => 'amis']);
	      
	  }

	  public function accepter($a)
	  {

	 	$this -> loadModel("Bonds");
		$article = $this->Bonds->newEntity();
		$article->id = $a;
		$article->trusted = 1;
	
	if ($this ->Bonds->save($article)) {
	return $this->redirect(['controller'=>'Bonds','action' =>'amis']);
    }
}

	public function supprimer($b){
		$this-> loadModel("Bonds");
	$this->request->allowMethod(['post', 'delete']);
	
    $bonds= $this->Bonds->get($b);
	if ($this->Bonds->delete($bonds)) {
		return $this->redirect(['controller'=>'Bonds','action' => 'amis']);
	}
	}

	public function Camis($bondid){
		
		$this -> loadModel("Members");
        $m = $this->Members->find("all", array("conditions"=>array("id"=>$bondid)));
        $this->set("member", $m->toArray());
	}
	  

  
}