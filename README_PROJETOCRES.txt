#Quelques indications sur le fonctionnement de notre site

1. Messages
	
	Dans la table message il y a un attribut "read"
Le mot "read" �tant r�serv� dans le SQL de CakePhp, nous ne pouvions pas mettre � jour la base de donn�e.
	
	Solution => Nous avons chang� localement le nom de l'attribu � "lu"
Merci de prendre en consid�ration ce changement lors de votre correction.


2. API

	Il faut aller chercher les ID dans la base de donn�e pour �x�cuter les API

	API workoutparameters & getsummary => il faut mettre .json � la fin de l'url pour que les requ�tes s'affichent comme il faut.

3. Connexion Facebook
	
	La connexion Facebook �tant bloqu�e en local nous avons perdu �norm�ment de temps sur ce point.
	Elle s'affiche donc en local mais ne fonctionne pas. 
	Merci de la tester en ligne.

	