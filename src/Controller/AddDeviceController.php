<?php
    namespace App\Controller;

    use App\Controller\AppController;

    class AddDeviceController extends AppController
    {
		public function addDevice($IDmember, $serial)
    {
      $this->loadModel("Devices");
	  $new = $this->Devices->newEntity();
	  $new->member_id = $IDmember;
	  $new->serial = $serial;
	  $new->description = 'En attente de validation';
	  $new->trusted = 0;
	  $this->Devices->save($new);
	  
	return $this->redirect(['controller'=>'Devices','action' => 'connected']);	}
}