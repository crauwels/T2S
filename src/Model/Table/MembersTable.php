<?php 
namespace App\Model\Table;


use Cake\ORM\Table;
use Cake\Validation\Validator;

class MembersTable extends Table
{
	public function initialize(array $config)
    {
		$this->setTable('Members');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
		
        $this->hasMany('Logs');
    }
	
	public function editPassword($password, $email, $id = null)
    {
      if($member=$this->find()->where(['email' => $email])->first()? $this->find()->where(['email' => $email])->first() : $this->find()->where(['id' => $id])->first())
      {
        $member->password = $password;
        if($this->save($member))
        {
          return true;
        }
      }
      return false;
    }
	
	public function editEmail($email, $id)
    {
      if($member=$this->find()->where(['id' => $id])->first())
      {
        $member->email = $email;
        if($this->save($member))
        {
          return true;
        }
      }
      return false;
    }
	
    public function validationDefault(Validator $validator)
    {
        return $validator
            ->notEmpty('email', "Un email est nécessaire")
            ->notEmpty('password', 'Un mot de passe est nécessaire');      
    }

}