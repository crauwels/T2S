
  <div class="nouveauMessage">

  <h2 class="titre">Démarrer une nouvelle discussion</h2>
  <?php 
  echo $this->Form->create('a', array('url'=>["controller"=>"Messages", "action"=>"envoyer/"])); ?>

  <div class="nouveauChat">

    <?= $this->Form->input("Destinataire");?>
    <?= $this->Form->input("Sujet");?>
    <?= $this->Form->input("Message");?>


    <div id="boutonAj"><?= $this->Form->submit("Envoyer");?></div>
    <?= $this->Form->end();?>
  </div>
    <div class="scroll-down" > 
     <a class="btn js-scroll-trigger js-scrollTo" style="    font-size: 45px;
    line-height: 70px;
    width: 70px;
    height: 70px;
    padding: 0;
    letter-spacing: normal;
    color: #ffe4f4;
    border: 2px solid #ffe4f4;
    border-radius: 100% !important;
    -webkit-transition: all 0.5s;
    transition: all 0.5s;" href="#ici"> 
          <i class="fa fa-angle-down fa-fw" ></i>
      </a>
    </div>
</div>


<div class="sujet" id ="ici">
  <h2 class="titre">Mes Messages</h2>
  <?php $nouveau = "";
  $premier = 1;
  $sujet = "";
  $i = 0;
  foreach($message as $m){$sujet = $m->name;
    $i++;
    if($nouveau != $m->name){
      if ($premier==0)
      { 
       if (sizeof($message)>0){
         echo' </ul>
         </div>
         <div class="panel-footer">'
         .$this->Form->create('a', array('url'=>["controller"=>"Messages", "action"=>"ajouter/".$sujet])).'
         <div class="input-group">'
         .$this->Form->input("Msg", ["label" => false,"placeholder"=>"Tape ton message ici...", "id"=>"btn-input", "class"=>"form-control input-sm"]).'
         <span class="input-group-btn">'.
         $this->Form->submit("Envoyer", ["class"=>"btn btn-warning btn-sm", "id"=>"btn-chat"]).'
         </span>

         </div>'
         .$this->Form->end()
         .'</div>
         </div>
         </div>
         </div>
         </div>
         ';}
       }
       $premier = 0;
       echo' 
       <div class="row">

       <div class="col-md-3"></div>
       <div class="col-md-6">
       <div class="panel" style="border-color: #ffc2e6;">
       <div class="panel-heading" id="accordion">
       <span class="glyphicon glyphicon-comment"></span> '.$m->name.'
       <div class="btn-group pull-right">
       <a type="button" class="btn btn-default btn-xs" data-toggle="collapse" data-parent="#accordion" href="#'.$i.'">
       <span class="glyphicon glyphicon-chevron-down"></span>
       </a>
       </div>
       </div>

       <div class="panel-collapse collapse" id="'.$i.'">
       <div class="panel-body">
       <ul class="chat">';
       $nouveau= $m->name;}
       if ($m->member_id == $moi){echo'
        <li class="right clearfix"><span class="chat-img pull-right">
      <img src="http://placehold.it/50/FA6F57/fff&text=ME" alt="User Avatar" class="img-circle" />
      </span>
      <div class="chat-body clearfix">
      <div class="header">
      <small class=" text-muted"><span class="glyphicon glyphicon-time"></span>'.$m->date.'</small>
      <strong class="pull-right primary-font">'; foreach($membre as $personne){if($m->member_id==$personne->id){ $emailCoupe = explode("@", $personne->email); echo$emailCoupe[0];}  }echo '</strong>
      </div>
      <p>
      '.$m->description.'
      </p>
      </div>
      </li>';}
      if($m->member_id != $moi){echo
        '<li class="left clearfix"><span class="chat-img pull-left">
        <img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" />
        </span>
        <div class="chat-body clearfix">
        <div class="header">
        <strong class="primary-font">';foreach($membre as $personne){if($m->member_id==$personne->id){ $emailCoupe = explode("@", $personne->email); echo$emailCoupe[0];}  }echo '</strong> <small class="pull-right text-muted">
        <span class="glyphicon glyphicon-time"></span>'.$m->date.'</small>
        </div>
        <p>
        '.$m->description.'
        </p>
        </div>
        </li>';}





      }?>
      <?php if (sizeof($message)>0): ?>
      </ul>
    </div>
    <div class="panel-footer">
      <?php echo $this->Form->create('a', array('url'=>["controller"=>"Messages", "action"=>"ajouter/".$sujet])); ?>
      <div class="input-group">
        <?= $this->Form->input("Msg", ["label" => false,"placeholder"=>"Tape ton message ici...", "id"=>"btn-input", "class"=>"form-control input-sm"]);?>
        <span class="input-group-btn">
         <?= $this->Form->submit("Envoyer", ["class"=>"btn btn-warning btn-sm", "id"=>"btn-chat"]);?>
       </span>

     </div>
     <?= $this->Form->end();?>
   </div>
 </div>
</div>
</div>
</div>

<?php endif; ?> 
</div>
<script>
  $(document).ready(function() {
    $('.js-scrollTo').on('click', function() { // Au clic sur un élément
      var page = $(this).attr('href'); // Page cible
      var speed = 750; // Durée de l'animation (en ms)
      $('html, body').animate( { scrollTop: $(page).offset().top }, speed ); // Go
      return false;
    });
  });
</script>