<div class="bckgrnd">
<br>

<section id="inscription">
    <?= $this->Form->create();?>

    <div class="container">
        <div class="row">
            <div class="text-center inscripform">
                <h2>Inscription</h2>
                <h4> Si tu fais partie des gens biens rejoins les jambons !</h4>
            </div>
        </div>

        <div class="row text-center form-group cadretransp"> <p></p>
            <?= $this->Form->input("email",array('type' => 'email', 'class' => 'pigframewb decalewat'));?> 
            <?= $this->Form->input("password",array('type' => 'password', 'class' => 'pigframewb'));?>
            <?= $this->Form->submit("Rejoindre la pig-communauté",array('class' => 'pigframe'));?>
            <?= $this->Form->end();?>
        </div>
        </div>
    </div>
</section>
</div>

<style>
    .bckgrnd
    {
        background-image: url('../img/connectpig.png');
        background-size:cover;
        padding-top: 50px;
        padding-bottom: 300px;  
    }
    .descripic{
        border: 1px solid #ddd;
        position:sticky;
        position: relative;
        background-attachment: fixed;
        max-width: 100%;
        height: auto;
        z-index:-1;
    }

    .inscripform{
        max-width: 300px;
        background: #FDDDE6;
        margin: auto;
        margin-bottom: 30px;
        box-shadow: 1px 1px 25px rgba(0, 0, 0, 0.35);
        border-radius: 10px;
        border: 6px solid #ffc2e6;
        position:relative;
        z-index:0;
        opacity:0,5;
    }
    .pigframe{
        background: #FDDDE6;
        box-shadow: 1px 1px 25px rgba(0, 0, 0, 0.35);
        border-radius: 10px;
        border: 6px solid #ffc2e6;
        margin: 10px 3px;
    }


    .pigframewb{
        background: white;
        box-shadow: 1px 1px 25px rgba(0, 0, 0, 0.35);
        border-radius: 3px;
        color: #FF69B4;
        object-position: center;
        margin-left:10px;
        width:80%;
        background-color: white;
            
    }

    .cadretransp{
        background: #FDDDE6;
        box-shadow: 1px 1px 25px rgba(0, 0, 0, 0.35);
        border-radius: 10px;
        border: 6px solid #ffc2e6;
        margin: 0 auto;
        max-width: 50%;
        opacity:0.9;
    }
    
    .decalewat{
        margin-left:40px;
    }

</style>

